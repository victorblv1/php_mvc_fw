<?php


namespace app\kernel;

/**
 * Class Response
 *
 * @author Viktors Belovs
 * @package app\kernel
 */
class Response {

    public function setStatusCode(int $code) {
        http_response_code($code);
    }

    public function redirect(string $url) {
        header('Location: ' . $url);
    }

}