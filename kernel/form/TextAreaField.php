<?php


namespace app\kernel\form;

/**
 * Class TextArea
 *
 * @author Viktors Belovs
 * @package app\kernel\form
 */
class TextAreaField extends BaseField {

    public function renderInput(): string {
        return sprintf('<textarea name="%s" class="form-control%s">%s</textarea>',
            $this->attribute,
            $this->model->hasError($this->attribute) ? ' is-invalid' : '',
            $this->model->{$this->attribute},
        );
    }

}