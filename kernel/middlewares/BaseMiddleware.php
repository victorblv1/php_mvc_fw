<?php


namespace app\kernel\middlewares;

/**
 * Class BaseMiddleware
 *
 * @author Viktors Belovs
 * @package app\kernel\middlewares
 */
abstract class BaseMiddleware {

    abstract public function execute();

}