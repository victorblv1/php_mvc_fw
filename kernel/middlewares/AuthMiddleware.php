<?php


namespace app\kernel\middlewares;

use app\kernel\Application;
use app\kernel\exception\ForbiddenException;

/**
 * Class AuthMiddleware
 *
 * @author Viktors Belovs
 * @package app\kernel\middlewares
 */
class AuthMiddleware extends BaseMiddleware {

    public array $actions = [];

    /**
     * AuthMiddleware constructor.
     *
     * @param array $actions
     */
    public function __construct(array $actions = []) {
        $this->actions = $actions;
    }

    /**
     * @throws ForbiddenException
     */
    public function execute() {
        if (Application::isGuest()) {
            if (empty($this->actions) || in_array(Application::$app->controller->action, $this->actions)) {
                throw new ForbiddenException();
            }
        }
    }

}