<?php


namespace app\kernel;


abstract class UserModel extends DbModel {
    abstract public function getDisplayedName(): string;
}