<?php


namespace app\kernel\exception;

/**
 * Class NotFoundException
 *
 * @author Viktors Belovs
 * @package app\kernel\exception
 */
class NotFoundException extends \Exception {
    protected $code = 404;
    protected $message = 'Page not found';
}