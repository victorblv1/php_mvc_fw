<?php


namespace app\kernel\exception;

/**
 * Class ForbiddenException
 *
 * @author Viktors Belovs
 * @package app\kernel\exception
 */
class ForbiddenException extends \Exception {

    protected $code = 403;
    protected $message = 'You do not have permission to access this page';
}