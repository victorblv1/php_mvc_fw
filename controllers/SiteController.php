<?php

namespace app\controllers;

use app\kernel\Application;
use app\kernel\Controller;
use app\kernel\Request;
use app\kernel\Response;
use app\models\ContactForm;

/**
 * Class SiteController
 *
 * @author Viktors Belovs
 * @package app\controllers
 */
class SiteController extends Controller {

    public function home() {
        $params = [
            'name' => 'viktorblv',
        ];
        return $this->render('home', $params);
    }

    public function contact(Request $request, Response $response) {
        $contact = new ContactForm();
        if ($request->isPost()) {
            $contact->loadData($request->getBody());
            if ($contact->validate() && $contact->send()) {
                Application::$app->session->setFlash('success', 'Thank you for contacting us. We will come back to you soon.');
                return $response->redirect('/contact');
            }
        }
        return $this->render('contact', [
            'model' => $contact,
        ]);
    }


}