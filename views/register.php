<?php
/** @var $model \app\models\User */
?>

<h1>Create a new account</h1>
<?php $form = \app\kernel\form\Form::begin('', 'post') ?>
  <div class="row">
    <div class="col">
        <?php echo $form->field($model, 'firstName') ?>
    </div>
    <div class="col">
        <?php echo $form->field($model, 'lastName') ?>
    </div>
  </div>
  <?php echo $form->field($model, 'email') ?>
  <?php echo $form->field($model, 'password')->passwordField() ?>
  <?php echo $form->field($model, 'confirmPassword')->passwordField() ?>
  <br>
  <button type="submit" class="btn btn-primary">Submit</button>
<?php \app\kernel\form\Form::end() ?>
