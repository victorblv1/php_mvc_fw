<?php
/** @var $model \app\models\User */
?>

<h1>Login</h1>
<?php $form = \app\kernel\form\Form::begin('', 'post') ?>
  <?php echo $form->field($model, 'email') ?>
  <?php echo $form->field($model, 'password')->passwordField() ?>
  <br>
  <button type="submit" class="btn btn-primary">Submit</button>
<?php \app\kernel\form\Form::end() ?>
