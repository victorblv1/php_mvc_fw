<?php
/** @var $this \app\kernel\View */
/** @var $model \app\models\ContactForm */

use app\kernel\form\TextAreaField;

$this->title = 'Contact';
?>

<h1>Contact us </h1>

<?php $form = \app\kernel\form\Form::begin('', 'post') ?>
<?php echo $form->field($model, 'subject') ?>
<?php echo $form->field($model, 'email') ?>
<?php echo new TextAreaField($model, 'body') ?>
<br>
<button type="submit" class="btn btn-primary">Submit</button>
<?php $form = \app\kernel\form\Form::end(); ?>
